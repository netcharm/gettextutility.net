/* Automatically generated by GNU msgfmt.  Do not modify!  */
using Gettext.WinForm;
public class GetTextUtils_zh_CHS : GettextResourceSet {
  public GetTextUtils_zh_CHS ()
    : base () {
  }
  protected override void ReadResources () {
    if (Table == null)
      Table = new System.Collections.Hashtable();
    System.Collections.Hashtable t = Table;
    t.Add("Ready", "就绪");
    t.Add("Patch File", "文件打补丁");
    t.Add("Add Code/FileRef to project\\n(not recommended for senior)", "添加所需的代码和参考库文件到项目\\n(熟练人员不推荐)");
    t.Add("Load Solution", "载入解决方案");
    t.Add("Load Solution file", "载入解决方案文件");
    t.Add("Create PO", "生成PO文件");
    t.Add("Create locale .PO file for every project", "为解决方案中的每个项目生成指定语言的本地化.PO文件");
    t.Add("Create DLL", "生成资源DLL");
    t.Add("Create locale ResourceDLL file", "为解决方案中的每个项目生成指定语言的本地化资源DLL文件");
    t.Add("Create POT", "生成POT文件");
    t.Add("Create .POT file for every project", "为解决方案中的每个项目生成指定语言的.POT模板文件");
    t.Add("Target Locale", "目标语言");
    t.Add("Dropdown to select locale from all system supported", "下拉从系统支持的全部语言中选择目标语言");
    t.Add("CHT", "繁体中文");
    t.Add("Most usage locale", "常用语言");
    t.Add("CHS", "简体中文");
    t.Add("Translating PO", "打开PO翻译");
    t.Add("Open .PO Editor to translating it to locale", "使用.PO关联的编辑器打开指定语言的本地化文件以便翻译");
    t.Add("State Info", "状态信息");
    t.Add("Solution File:", "解决方案文件:");
    t.Add("Logger:", "输出信息显示:");
    t.Add("Browsing Directory to select Solution file.", "浏览目录以选择解决方案文件");
    t.Add("Solution File|*.sln|All Files|*.*", "解决方案文件|*.sln|所有文件|*.*");
    t.Add("Open Solution File", "打开解决方案文件");
    t.Add("GNU GetText Utility", "GNUGetText for Microsoft.Net C# 辅助小工具");
    t.Add("Solution must be loaded first!", "必须先载入解决方案文件!");
    t.Add("Loaded", "已载入");
  }
}

